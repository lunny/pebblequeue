// Copyright 2020 Andrew Thornton. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.
package levelqueue

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSet(t *testing.T) {
	const dbDir = "./set"
	os.RemoveAll(dbDir)

	set, err := OpenSet(dbDir)
	assert.NoError(t, err)

	// SAdd
	added, err := set.Add([]byte("test1"))
	assert.NoError(t, err)
	assert.EqualValues(t, true, added)

	added, err = set.Add([]byte("test2"))
	assert.NoError(t, err)
	assert.EqualValues(t, true, added)

	added, err = set.Add([]byte("test1"))
	assert.NoError(t, err)
	assert.EqualValues(t, false, added)

	added, err = set.Add([]byte("test3"))
	assert.NoError(t, err)
	assert.EqualValues(t, true, added)

	added, err = set.Add([]byte("test5"))
	assert.NoError(t, err)
	assert.EqualValues(t, true, added)

	// SRem
	removed, err := set.Remove([]byte("test1"))
	assert.NoError(t, err)
	assert.EqualValues(t, true, removed)

	removed, err = set.Remove([]byte("test4"))
	assert.NoError(t, err)
	assert.EqualValues(t, false, removed)

	removed, err = set.Remove([]byte("test3"))
	assert.NoError(t, err)
	assert.EqualValues(t, true, removed)

	// SIsMember
	isMember, err := set.Has([]byte("test2"))
	assert.NoError(t, err)
	assert.EqualValues(t, true, isMember)

	isMember, err = set.Has([]byte("test1"))
	assert.NoError(t, err)
	assert.EqualValues(t, false, isMember)

	// SMembers
	members, err := set.Members()
	assert.NoError(t, err)
	assert.Contains(t, members, []byte("test2"))
	assert.Contains(t, members, []byte("test5"))
	assert.NotContains(t, members, []byte("test1"))
	assert.NotContains(t, members, []byte("test3"))
	assert.NotContains(t, members, []byte("test4"))
}
