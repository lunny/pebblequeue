module gitea.com/lunny/levelqueue

require (
	github.com/cockroachdb/pebble v0.0.0-20220303004947-02363a5592ff
	github.com/stretchr/testify v1.6.1
)

go 1.13
